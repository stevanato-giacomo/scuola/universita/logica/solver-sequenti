# Solver di sequenti

Programma che esegue la derivazione dei sequenti e li classifica in tautologie, paradossi o opinioni.
Nel caso il sequente non sia una tautologia viene derivato anche la sua negazione.
Nel caso di opinioni trova anche due combinazioni di valori, una in cui il sequente originale è vero e una in cui è falso.

L'output è organizzato diversamente dal layout visto in classe. Il sequente originale è posto in cima e l'albero si sviluppa verso il basso, con ogni linea occupata da un altro sequente o una foglia. Sulla destra viene scritta la regola usata per passare al sequente/i successivi, l'assioma individuato oppure se non è possibile derivare nulla.

# Esecuzione

Il programma è scritto in [rust](https://www.rust-lang.org/) e richiede il suo compilatore. Una volta installato per eseguire il programma (in versione debug) basta usare il comando:
    cargo run
Invece per compilare il programma in modalità release (ovvero ottimizzato) basta usare il comando:
    cargo build --release

Si pianifica in futuro di reimplementare il programma in un'app Android.

# Input 

Il programma cerca di leggere l'intera stringa di input come un sequente, senza lasciare nessun carattere non letto alla fine.

Gli spazi sono completamente facoltativi e non modificano l'output del programma.

## Atomo

Un atomo è una lettera dell'alfabeto maiuscola esclusa la `V` perché viene usata come simbolo di `V`.

## Vero e Falso

I simboli di "vero" accettati sono `1` e `tt`.
I simboli di "falso" accettati sono `0` e `⊥`.

## Not

I simboli di "not" accettati sono `¬`, `!` e `~`.

Il not è l'operazione con la precedenza maggiore. Quando viene composto con degli atomi oppure vero/falso non è necessario inserirlo tra parentesi, ad esempio `¬A & B` è lo stesso di `(¬A) & B`, mentre per negare tutto l'and è necessario usare le parentesi `¬(A & B)`.

## And

I simboli di "and" accettati sono `&` e `∧`.

Ha una precedenza inferiore al not e uguale all'or. È quindi necessario aggiungere delle parentesi agli operandi almeno che questi non siano degli atomi, vero/falso oppure la loro negazione. È necessario aggiungere delle parentesi anche nel caso gli operandi siano degli and o degli or.

Esempi:
 - `A & B`
 - `A ∧ B`
 - `(A & B) & C` (notare come sono necessarie le parentesi)
 - `¬A & ¬B` (non sono necessarie le parentesi)

## Or

I simboli di "or" accettati sono `V` e `∨`.

Ha la stessa precedenza dell'and ed è del tutto analogo ad esso (eccetto ovviamente il significato).

## Implica

I simboli di "implica" accettati sono `->` e `→`.

Ha una precedenza inferiore all'and e all'or, pertanto è necessario aggiungere delle parentesi solo nel caso in cui agli operandi siano presenti altri implica.

Esempi:
 - `A & B -> B & A` equivale a `(A & B) -> (B & A)`
 - `(A -> B) -> C` e `A -> (B -> C)` richiedono entrambi le parentesi.

## Sequente

I simboli di "sequente" accettati sono `⊢` e `|-`.

Un sequente viene identificato da due liste di proposizioni separate dal simbolo di sequente. Le proposizioni sono separate da virgole (`,`) e non è necessario aggiungere inserirle all'interno di parentesi. Le liste possono anche essere vuote.

Il simbolo di sequente può essere assente, in questo caso l'input verrà letto come una proposizione singola non nulla e verrà derivata ponendola alla destra del simbolo di sequente. In questo modo il sequente avrà la stessa verità della proposizione immessa.

Esempi:
 - `A & B, C, D |- E, F -> D`
 - `A & B |-` è composto da solo una proposizione a sinistra e nessuna a destra ed è accettato
 - `|- A & B` è composto da solo una proposizione a destra e nessuna a sinistra ed è accettato
 - `|-` è il sequente vuoto ed è accettato
 - `A & B` equivale al sequente `|- A & B`

Immagine di esempio:

![Immagine di esempio](./example-output.jpg)
