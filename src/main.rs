mod sequente;

use sequente::Sequente;
use std::convert::TryFrom;

fn main() -> Result<(), String> {

    print!("Inserire un sequente: ");
    let mut input = String::new();
    read_string(&mut input)?;

    // Esegue il parse del sequente ed esegue l'output della sua derivazione
    let seq = Sequente::try_from(input.as_str())?;
    let der = seq.derivazione();
    println!("Derivazione del sequente:");
    println!("{}", der);

    // Verifica se il sequente è una tautologia, in caso contrario verifica se è un paradosso o un'opinione
    if der.is_tautologia() {
        println!("Il sequente è una tautologia");
    }
    else {
        // Calcola il sequente negato ed effettua la sua derivazione
        let negato = der.seq.negato();
        let der_negato = negato.derivazione();
        println!("Derivazione del negato:");
        println!("{}", der_negato);
        // Se il sequente negato è una tautologia allora quello originale è un paradosso. Al contrario è un'opinione
        if der_negato.is_tautologia() {
            println!("Il sequente originale è un paradosso");
        } else {
            println!("Il sequente originale è un'opinione");
            println!("È vero ad esempio se {}", der_negato.falso_quando().unwrap());
            println!("È falso ad esempio se {}", der.falso_quando().unwrap());
        }
    }

    Ok(())
}

// Legge una stringa dall'input e la scrive in target
fn read_string(target: &mut String) -> Result<(), String> {
    use std::io::{stdin,stdout,Write};
    // Assicura che l'output scritto prima dell'input sia mostrato
    let _ = stdout().flush();
    // Legge dall'input 
    stdin().read_line(target).map_err(|_| "Errore durante l'acquisizione dell'input")?;
    // Rimuove i caratteri di 'a capo' dalla fine della stringa
    if let Some('\n') = target.chars().next_back() { target.pop(); }
    if let Some('\r') = target.chars().next_back() { target.pop(); }
    Ok(())
}