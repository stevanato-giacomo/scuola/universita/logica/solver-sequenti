use pest::Parser;
use std::convert::TryFrom;
use super::proposizione::Proposizione;
use super::parser::*;

// Sequente rappresentato da due liste di preposizioni,
// una con quelle a sinistra e l'altra con quelle a destra del simbolo di sequente.
pub struct Sequente {
    pub props_sx: Vec<Proposizione>,
    // NB: L'ordine del vettore destro è invertito (l'ultimo elemento è quello letto per primo)
    //     Lo scopo è quello di rendere più efficiente il `pop` dell'elemento più vicino al simbolo di sequente
    pub props_dx: Vec<Proposizione>
}

impl Sequente {
    // Ritorna una proposizione equivalente all'& tra ogni proposizione presente nella lista di sinistra
    fn gamma_e(&self) -> Proposizione {
        self.props_sx.iter().skip(1).fold(
            self.props_sx[0].clone(),
            |o, n| Proposizione::And(Box::new(o), Box::new(n.clone()))
        )
    }

    // Ritorna una proposizione equivalente all'∨ tra ogni proposizione presente nella lista di destra
    fn delta_o(&self) -> Proposizione {
        self.props_dx.iter().rev().skip(1).fold(
            self.props_dx.last().unwrap().clone(),
            |o, n| Proposizione::Or(Box::new(o), Box::new(n.clone()))
        )
    }

    // Converte il sequente nel sequente della sua negazione
    pub fn negato(&self) -> Self {
        match (self.props_sx.len(), self.props_dx.len()) {
            (0, 0) => Self {
                props_sx: vec![Proposizione::Falso],
                props_dx: vec![],
            },
            (0, _) => Self {
                props_sx: vec![self.delta_o()],
                props_dx: vec![],
            },
            (_, 0) => Self {
                props_sx: vec![],
                props_dx: vec![self.gamma_e()],
            },
            (_, _) => Self {
                props_sx: vec![self.delta_o()],
                props_dx: vec![self.gamma_e()],
            }
        }
    }
}

impl TryFrom<&str> for Sequente {
    type Error = String;

    // Converte la stringa `input` in un Sequente, se possibile
    fn try_from(input: &str) -> Result<Self, Self::Error> {
        let mut sequente_pairs = ParserSequente::parse(Rule::sequente, input)
            .map_err(|_| "Sequente non valido")?
            .next() // Ritorna il primo (e unico in questo caso) elemento della lista, ovvero il sequente stesso
            .unwrap()
            .into_inner(); // Ritorna la lista degli elementi del sequente (ovvero le due liste di proposizioni)
        
        let pair_list_1 = sequente_pairs.next().unwrap();

        let (props_sx, props_dx) = if let Some(pair_list_2) = sequente_pairs.next() {
            (
                pair_list_1.into_inner().map(|p| Proposizione::from(p)).collect(),
                pair_list_2.into_inner().map(|p| Proposizione::from(p)).rev().collect()
            )
        } else {
            (
                Vec::new(),
                pair_list_1.into_inner().map(|p| Proposizione::from(p)).rev().collect()
            )
        };

        let sequente = Sequente { props_sx, props_dx };

        Ok(sequente)
    }
}
