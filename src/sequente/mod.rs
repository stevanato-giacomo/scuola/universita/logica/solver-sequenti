// modulo della cartella `sequente`
// Raccoglie e rende visibili i sottomoduli contenuti nei singoli file

mod sequente;
mod parser;
mod proposizione;
mod derivazione;
mod display;

pub use self::sequente::Sequente;