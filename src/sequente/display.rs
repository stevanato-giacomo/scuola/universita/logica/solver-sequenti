use colored::*;
use std::fmt::Display;
use super::derivazione::{AlberoDerivazione, Derivazione, Regola};
use super::proposizione::Proposizione;
use super::sequente::Sequente;

impl Display for Sequente {
    // Scrive il sequente su `f`
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.props_sx.len() != 0 {
            write!(f, "{}", self.props_sx[0])?;
            for prop in self.props_sx.iter().skip(1) {
                write!(f, ", {}", prop)?;
            }
            write!(f, " ")?;
        }
        write!(f, "⊢")?;

        if self.props_dx.len() != 0 {
            write!(f, " {}", self.props_dx.last().unwrap())?;
            for prop in self.props_dx.iter().rev().skip(1) {
                write!(f, ", {}", prop)?;
            }
        }

        std::fmt::Result::Ok(())
    }
}


impl Derivazione {
    // Simile a `fmt` ma tiene in considerazione il prefisso e scrive anche una linea rappresentante la gerarchia (?) della derivazione
    fn fmt_ramo(&self, f: &mut std::fmt::Formatter, prefisso: String, ultimo: bool) -> std::fmt::Result {
        write!(f,
            "\n{prefisso}{linea} {sequente}  {regola}",
            prefisso = prefisso.cyan().bold(),
            linea = (if ultimo { "└──▪" } else { "├──▪" }).cyan().bold(),
            sequente = self.seq,
            regola = self.regola)?;
        // Se non è l'ultimo ramo aggiunge una linea nel prefisso che si congiungerà a quella dell'ultimo ramo
        let prefisso = prefisso + if ultimo { "   " } else { "│  " };
        match &self.albero {
            AlberoDerivazione::Ramo(d) => {
                d.fmt_ramo(f, prefisso, true)?;
            },
            AlberoDerivazione::Rami(d1, d2) => {
                d1.fmt_ramo(f, prefisso.clone(), false)?;
                d2.fmt_ramo(f, prefisso, true)?;
            },
            _ => {},
        }

        Ok(())
    }
}

impl Display for Derivazione {
    // Scrive le derivazione su `f`
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f,
            "{q} {sequente}  {regola:#}",
            q = "▪".cyan().bold(),
            sequente = self.seq,
            regola = self.regola)?;
        // La scrittura dei rami avviene tramite una funzione aiutante
        // Questo è necessario per tenere in considerazione il prefisso e se il ramo è l'ultimo
        match &self.albero {
            AlberoDerivazione::Ramo(d) => {
                d.fmt_ramo(f, String::new(), true)?;
            },
            AlberoDerivazione::Rami(d1, d2) => {
                d1.fmt_ramo(f, String::new(), false)?;
                d2.fmt_ramo(f, String::new(), true)?;
            },
            _ => {},
        }

        Ok(())
    }
}

impl Display for Regola {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use Regola::*;
        let mut regola_str = match self {
            AxId => "ax-id",
            AxF => "ax-⊥",
            AxT => "ax-tt",
            ScSx => "sc-sx",
            ScDx => "sc-dx",
            AndS => "&-S",
            AndD => "&-D",
            OrS => "∨-S",
            OrD => "∨-D",
            NotS => "¬-S",
            NotD => "¬-D",
            ImplicaS => "→-S",
            ImplicaD => "→-D",
            NonAssioma => "Non è derivabile né un assioma"
        }.normal();
        regola_str = format!("[{}]", regola_str).bold();
        regola_str = match self {
                AxId | AxF | AxT => regola_str.green(),
                NonAssioma => regola_str.red(),
                _ => regola_str.yellow(),
            };
        write!(f, "{}", regola_str)
    }
}

impl Display for Proposizione {
    // Scrive la proposizione su `f`
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use Proposizione::*;

        fn write_or_and_implica(f: &mut std::fmt::Formatter<'_>, p: &Box<Proposizione>) -> std::fmt::Result {
            match **p {
                And(_,_) | Or(_,_) | Implica(_,_) => write!(f, "({})", **p)?,
                _ => write!(f, "{}", **p)?,
            }
            std::fmt::Result::Ok(())
        }

        match self {
            Vero => write!(f, "tt")?,
            Falso => write!(f, "⊥")?,
            Atomo(c) => write!(f, "{}", c)?,
            Not(a) => {
                write!(f, "¬")?;
                write_or_and_implica(f, a)?;
            },
            Or(a,b) => {
                write_or_and_implica(f, a)?;
                write!(f, "∨")?;
                write_or_and_implica(f, b)?;
            },
            And(a,b) => {
                write_or_and_implica(f, a)?;
                write!(f, "&")?;
                write_or_and_implica(f, b)?;
            },
            Implica(a,b) => {
                if let Implica(_,_) = **a { write!(f, "({})", **a)?; }
                else { write!(f, "{}", **a)?; }
                write!(f, "→")?;
                if let Implica(_,_) = **b { write!(f, "({})", **b)?; }
                else { write!(f, "{}", **b)?; }
            }
        }
        std::fmt::Result::Ok(())
    }
}
