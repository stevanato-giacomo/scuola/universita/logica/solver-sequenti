use super::sequente::Sequente;
use super::proposizione::Proposizione;

// Albero binario non bilanciato rappresentate la derivazione del sequente `seq`
pub struct Derivazione {
    // Sequente cui viene fatta la derivazione
    pub seq: Sequente,
    // Regola usata per la derivazione, se derivabile
    pub regola: Regola,
    // Resto dell'albero di derivazione, se presente
    pub albero: AlberoDerivazione,
}

impl Derivazione {
    // Verifica se il sequente è una tautologia
    pub fn is_tautologia(&self) -> bool {
        match &self.albero {
            // Se il sequente è una foglia, verifica se il sequente stesso è un assioma o non è derivabile.
            AlberoDerivazione::Niente => match &self.regola {
                Regola::AxId | Regola::AxF | Regola::AxT => true,
                Regola::NonAssioma => false,
                _ => unreachable!()
            }
            // Se il sequente è derivabile, allora è una tautologia solo se i rami sono delle tautologie
            AlberoDerivazione::Ramo(d) => d.is_tautologia(),
            AlberoDerivazione::Rami(d1, d2) => d1.is_tautologia() && d2.is_tautologia()
        }
    }

    // Ritorna una stringa riportante un caso in cui il sequente è falso
    pub fn falso_quando(&self) -> Option<String> {
        fn join_eq(props: &Vec<Proposizione>) -> String {
            props.iter().map(|prop| match prop {
                Proposizione::Atomo(c) => c.to_string(),
                _ => unreachable!()
            }).collect::<Vec<String>>().join("=")
        }

        match &self.albero {
            AlberoDerivazione::Niente => match &self.regola {
                Regola::NonAssioma => Some({
                    let mut out = String::new();
                    if self.seq.props_sx.len() > 0 {
                        out += &join_eq(&self.seq.props_sx);
                        out += "=1";
                    }
                    if self.seq.props_sx.len() > 0 && self.seq.props_dx.len() > 0 {
                        out += " e ";
                    }
                    if self.seq.props_dx.len() > 0 {
                        out += &join_eq(&self.seq.props_dx);
                        out += "=0";
                    }
                    out
                }),
                _ => None
            },
            AlberoDerivazione::Ramo(d) => d.falso_quando(),
            AlberoDerivazione::Rami(d1, d2) => d1.falso_quando().or_else(|| d2.falso_quando())
        }
    }
}
// Rappresenta i possibili sviluppi di un nodo dell'albero della derivazione
pub enum AlberoDerivazione {
    Niente,
    Ramo(Box<Derivazione>),
    Rami(Box<Derivazione>, Box<Derivazione>)
}

// Regole usate nella derivazione
pub enum Regola {
    AxId,
    AxF,
    AxT,
    ScSx,
    ScDx,
    AndS,
    AndD,
    OrS,
    OrD,
    NotS,
    NotD,
    ImplicaS,
    ImplicaD,
    NonAssioma
}

impl Sequente {
    // Consuma il sequente e genera una derivazione per esso
    pub fn derivazione(self) -> Derivazione {
        let (albero, regola) = self.albero_derivazione_regola();
        Derivazione {
            seq: self,
            albero,
            regola
        }
    }
    // Genera lo sviluppo del sequente e la regola usata nel primo passaggio
    fn albero_derivazione_regola(&self) -> (AlberoDerivazione, Regola) {
        use AlberoDerivazione::*;
        use Regola::*;
        // Assiomi
        for prop_sx in &self.props_sx {
            // ax-⊥
            if let Proposizione::Falso = prop_sx {
                return (Niente, AxF);
            }
            // ax-id
            if self.props_dx.iter().any(|prop_dx| prop_sx == prop_dx) {
                return (Niente, AxId);
            }
        }
        // ax-tt
        for prop_dx in &self.props_dx {
            if let Proposizione::Vero = prop_dx {
                return (Niente, AxT);
            }
        }

        // &-S
        if let Some(Proposizione::And(a,b)) = self.props_sx.last() {
            let mut props_sx = self.props_sx.clone();
            props_sx.pop();
            props_sx.push(*a.clone());
            props_sx.push(*b.clone());
            return (Ramo(Box::new(Self { props_sx, props_dx: self.props_dx.clone() }.derivazione())), AndS);
        }
        // V-D
        if let Some(Proposizione::Or(a,b)) = self.props_dx.last() {
            let mut props_dx = self.props_dx.clone();
            props_dx.pop();
            props_dx.push(*a.clone());
            props_dx.push(*b.clone());
            return (Ramo(Box::new(Self { props_sx: self.props_sx.clone(), props_dx }.derivazione())), OrD);
        }
        // ¬-S
        if let Some(Proposizione::Not(a)) = self.props_sx.last() {
            let mut props_sx = self.props_sx.clone();
            let mut props_dx = self.props_dx.clone();
            props_sx.pop();
            props_dx.push(*a.clone());
            return (Ramo(Box::new(Self { props_sx, props_dx }.derivazione())), NotS);
        }
        // ¬-D
        if let Some(Proposizione::Not(a)) = self.props_dx.last() {
            let mut props_sx = self.props_sx.clone();
            let mut props_dx = self.props_dx.clone();
            props_dx.pop();
            props_sx.push(*a.clone());
            return (Ramo(Box::new(Self { props_sx, props_dx }.derivazione())), NotD);
        }
        // ->-D
        if let Some(Proposizione::Implica(a,b)) = self.props_dx.last() {
            let mut props_sx = self.props_sx.clone();
            let mut props_dx = self.props_dx.clone();
            props_dx.pop();
            props_dx.push(*b.clone());
            props_sx.push(*a.clone());
            return (Ramo(Box::new(Self { props_sx, props_dx }.derivazione())), ImplicaD);
        }

        // &-D
        if let Some(Proposizione::And(a,b)) = self.props_dx.last() {
            let mut props_dx_1 = self.props_dx.clone();
            props_dx_1.pop();
            props_dx_1.push(*a.clone());
            let mut props_dx_2 = self.props_dx.clone();
            props_dx_2.pop();
            props_dx_2.push(*b.clone());
            return (Rami(
                Box::new(Self{ props_sx: self.props_sx.clone(), props_dx: props_dx_1 }.derivazione()),
                Box::new(Self{ props_sx: self.props_sx.clone(), props_dx: props_dx_2 }.derivazione())
            ), AndD);
        }
        // V-S
        if let Some(Proposizione::Or(a,b)) = self.props_sx.last() {
            let mut props_sx_1 = self.props_sx.clone();
            props_sx_1.pop();
            props_sx_1.push(*a.clone());
            let mut props_sx_2 = self.props_sx.clone();
            props_sx_2.pop();
            props_sx_2.push(*b.clone());
            return (Rami(
                Box::new(Self{ props_sx: props_sx_1, props_dx: self.props_dx.clone() }.derivazione()),
                Box::new(Self{ props_sx: props_sx_2, props_dx: self.props_dx.clone() }.derivazione())
            ), OrS);
        }
        // ->-S
        if let Some(Proposizione::Implica(a,b)) = self.props_sx.last() {
            let mut props_dx_1 = self.props_dx.clone();
            props_dx_1.push(*a.clone());
            let mut props_sx_1 = self.props_sx.clone();
            props_sx_1.pop();
            let mut props_sx_2 = props_sx_1.clone();
            props_sx_2.push(*b.clone());
            return (Rami(
                Box::new(Self{ props_sx: props_sx_1, props_dx: props_dx_1 }.derivazione()),
                Box::new(Self{ props_sx: props_sx_2, props_dx: self.props_dx.clone() }.derivazione())
            ), ImplicaS);
        }

        //sc-sx
        for i in 0..self.props_sx.len() {
            use Proposizione::*;
            match self.props_sx[0] {
                Not(_) | And(_, _) | Or(_, _) | Implica(_, _) => {
                    let mut props_sx = self.props_sx.clone();
                    props_sx.swap(i, self.props_sx.len()-1);
                    return (Ramo(Box::new(Self{ props_sx, props_dx: self.props_dx.clone() }.derivazione())), ScSx)
                }
                _ => {},
            }
        }

        //sc-dx
        for i in 0..self.props_dx.len() {
            use Proposizione::*;
            match self.props_dx[0] {
                Not(_) | And(_, _) | Or(_, _) | Implica(_, _) => {
                    let mut props_dx = self.props_dx.clone();
                    props_dx.swap(i, self.props_dx.len()-1);
                    return (Ramo(Box::new(Self{ props_sx: self.props_sx.clone(), props_dx }.derivazione())), ScDx)
                }
                _ => {},
            }
        }

        // Non è possibile derivare il sequente
        (Niente, NonAssioma)
    }
}
