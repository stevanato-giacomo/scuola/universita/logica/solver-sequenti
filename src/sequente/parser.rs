use pest_derive::Parser;

// Parser autogenerato da pest a partire dalla grammatica di un sequente
#[derive(Parser)]
#[grammar="sequente/grammatica_sequente.pest"]
pub struct ParserSequente {}
