use pest::iterators::Pair;
use super::parser::*;

// Rappresenta una proposizione all'interno di un sequente
#[derive(Clone, PartialEq, Eq)]
pub enum Proposizione {
    Vero,
    Falso,
    Atomo(char),
    Not(Box<Proposizione>),
    And(Box<Proposizione>, Box<Proposizione>),
    Or(Box<Proposizione>, Box<Proposizione>),
    Implica(Box<Proposizione>, Box<Proposizione>),
}

impl From<Pair<'_, Rule>> for Proposizione {
    // Converte una corrispondenza del parser in una proposizione
    fn from(pair: Pair<'_, Rule>) -> Self {
        use Proposizione::*;

        let rule = pair.as_rule().clone(); // La regola che ha dato una corrispondenza
        let first_char = pair.as_str().chars().next().unwrap(); // Il primo carattere della corrispondenza
        let mut inner = pair.into_inner(); // Le sottocorrispondenze 

        match rule {
            Rule::proposizione | Rule::operando => Proposizione::from(inner.next().unwrap()),

            Rule::implica => Implica(
                Box::new(Proposizione::from(inner.next().unwrap())),
                Box::new(Proposizione::from(inner.next().unwrap()))
            ),
            Rule::and => And(
                Box::new(Proposizione::from(inner.next().unwrap())),
                Box::new(Proposizione::from(inner.next().unwrap()))
            ),
            Rule::or => Or(
                Box::new(Proposizione::from(inner.next().unwrap())),
                Box::new(Proposizione::from(inner.next().unwrap()))
            ),
            Rule::not => Not(Box::new(Proposizione::from(inner.next().unwrap()))),

            Rule::vero => Vero,
            Rule::falso => Falso,

            Rule::atomo => Atomo(first_char),

            rule => { unreachable!("{:?} non ammessa", rule); },
        }
    }
}
